import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JTextField;

public class test {
	
  public static void main(String[] args) {
    JFrame aWindow = new JFrame("KeyListener Tester");

    aWindow.setBounds(50, 100, 300, 300);
    aWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    JTextField typingArea = new JTextField(20);
    typingArea.addKeyListener(new KeyListener() {

      /** Handle the key typed event from the text field. */
      public void keyTyped(KeyEvent e) {
    	  System.out.println("You type : " + e.getKeyChar() + ", soit en ASCII :" + e.getKeyCode());

      }

      /** Handle the key pressed event from the text field. */
      public void keyPressed(KeyEvent e) {
    	  System.out.println("You press : " + e.getKeyChar() + ", soit en ASCII :" + e.getKeyCode());
      }

      /** Handle the key released event from the text field. */
      public void keyReleased(KeyEvent e) {
    	  System.out.println("You release : " + e.getKeyChar() + ", soit en ASCII :" + e.getKeyCode());
      }
    });
    aWindow.add(typingArea);
    aWindow.setVisible(true);
  }
    
}

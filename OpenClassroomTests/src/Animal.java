abstract class Animal {

    protected int poids;
    protected String couleur;

    protected void manger(){
        System.out.println("Je mange de la viande.");
    }
    protected void boire() {
        System.out.println("Je bois de l'eau.");
    }
    abstract void deplacement();
    abstract void crier();

    public interface I1{
        void faireCalin();
        void faireBeau();
        void lechouille();

    }

    public String toString(){
        String str = "Je suis un objet de la classe " + this.getClass().getName() +", je suis " + this.couleur + ", je pèse " + this.poids;
        return str;
    }
}

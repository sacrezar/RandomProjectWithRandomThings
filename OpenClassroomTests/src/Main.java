public class Main {


    public static void main(String[] args) {

        //Les méthodes d'un chien
        Chien c = new Chien("Gris bleuté", 20);
        c.boire();
        c.manger();
        c.deplacement();
        c.crier();
        System.out.println(c.toString());

        System.out.println("--------------------------------------------");
        //Les méthodes de l'interface
        c.faireCalin();
        c.faireBeau();
        c.lechouille();

        System.out.println("--------------------------------------------");
        //Utilisons le polymorphisme de notre interface
        Animal.I1 r = new Chien();
        r.faireBeau();
        r.faireCalin();
        r.lechouille();
    }

}

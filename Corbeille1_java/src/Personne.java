
public class Personne {
	
	private String nom = "";
	private String prenom = "";
	
	public void saluer() {
		System.out.println( (subs.getHour() < 18 && subs.getHour() > 5 ) ? 
				"Bonjour, je m'appelle " + this.nom + " " + this.prenom : "Bonsoir, je m'appelle " + this.nom + " " + this.prenom);	
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}	
}

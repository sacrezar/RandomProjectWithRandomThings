package helloworldmvc.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class DAOHelloWorld {
	
	private static String fileName = "HelloWorld.txt";
	private static DAOHelloWorld instance = null;
	public String message = null;
	
	public DAOHelloWorld() throws IOException {
		this.readFile();
	}

	private void readFile() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();
			while(line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			message = sb.toString();
		} finally {
			br.close();
			this.setMessage(message);
		}
	}
	

	public static DAOHelloWorld getInstance() {
		return instance;
	}

	public static void setInstance(DAOHelloWorld instance) {
		DAOHelloWorld.instance = instance;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}

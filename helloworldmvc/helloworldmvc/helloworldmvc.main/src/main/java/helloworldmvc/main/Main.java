package helloworldmvc.main;

import helloworldmvc.controller.Controller;
import helloworldmvc.model.Model;
import helloworldmvc.view.View;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final View view = new View();
		final Model model = new Model();
		final Controller ct = new Controller(view,model);
		ct.run();
	}

}
